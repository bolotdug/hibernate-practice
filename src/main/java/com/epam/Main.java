package com.epam;

import com.epam.models.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javax.rmi.CORBA.Stub;

public class Main {
    public static void main(String[] args) {

        Configuration configuration = new Configuration();
        configuration.configure("hibernate/hibernate.cfg.xml");
//        configuration.addAnnotatedClass(Student.class);

        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();

        session.beginTransaction();

        // student in TRANSIENT condition
        Student student = new Student();
        student.setName("Boris");
        student.setAge(30);

        // student in MANAGED condition
        session.persist(student);

        System.out.println("student persisted");
        student.setAge(15);

        session.detach(student);

        student.setAge(45);


        session.getTransaction().commit();

        // student in DETACHED
        session.close();
        sessionFactory.close();



    }
}
