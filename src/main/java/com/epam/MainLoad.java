package com.epam;

import com.epam.models.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class MainLoad {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate/hibernate.cfg.xml");

        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();

//        session.beginTransaction();
        Student student = session.get(Student.class, 2L);
        List<Student> students = session.createQuery("SELECT s FROM Student s", Student.class).getResultList();
        System.out.println(students);
        System.out.println(student.getName());


        session.refresh(student);
        System.out.println(student);



//        session.getTransaction().commit();

        session.close();
        sessionFactory.close();
    }
}
