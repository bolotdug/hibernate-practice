package com.epam;

import com.epam.models.Book;
import com.epam.models.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.Arrays;

public class MainWithEntities {
    public static void main(String[] args) {

        Configuration configuration = new Configuration();
        configuration.configure("hibernate/hibernate.cfg.xml");
//        configuration.addAnnotatedClass(Student.class);

        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();

        session.beginTransaction();

        Book book1 = new Book();
        book1.setName("Azbuka");

        session.persist(book1);

        Student student = new Student();
        student.setName("Boris");
        student.setAge(30);
        student.setBooks(Arrays.asList(book1));

        session.persist(student);

        session.getTransaction().commit();

        // student in DETACHED
        session.close();
        sessionFactory.close();



    }
}
